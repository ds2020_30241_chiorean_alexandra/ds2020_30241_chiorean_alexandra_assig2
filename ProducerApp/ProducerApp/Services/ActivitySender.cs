﻿using Newtonsoft.Json;
using ProducerApp.Model;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProducerApp.Services
{
    public class ActivitySender
    {
        private readonly string _hostname = "fox.rmq.cloudamqp.com";
        private readonly string _queueName = "ActivityQueue";
        private readonly string _username = "qinhaarc";
        private readonly string _password = "r_wdyHoH7HwqlJJKKJjiSiKNxHG9nu_a";

        public void SendActivity(Activity activity)
        {
            var factory = new ConnectionFactory() { HostName = _hostname, UserName = _username, Password = _password, VirtualHost = _username };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: _queueName, durable: false, exclusive: false, autoDelete: false, arguments: null);

                var json = JsonConvert.SerializeObject(activity);
                var body = Encoding.UTF8.GetBytes(json);

                channel.BasicPublish(exchange: "", routingKey: _queueName, basicProperties: null, body: body);
            }
        }
    }
}
