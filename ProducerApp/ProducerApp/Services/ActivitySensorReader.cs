﻿using ProducerApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ProducerApp.Services
{
    public class ActivitySensorReader
    {
        private string filePath = @"E:\Anul4\SD\Assignment2\activity.txt";
        private int PatientId = 2;

        public void ReadActivity()
        {
            int milliseconds = 1000;
            string line;
            ActivitySender sender = new ActivitySender();
            System.IO.StreamReader file = new System.IO.StreamReader(this.filePath);
            while ((line = file.ReadLine()) != null)
            {
                System.Console.WriteLine(line);
                string[] items = line.Split("\t\t");
                DateTime startDate = DateTime.ParseExact(items[0], "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                DateTime endDate = DateTime.ParseExact(items[1], "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                var activity = new Activity(PatientId, startDate, endDate, items[2]);
                sender.SendActivity(activity);
                Thread.Sleep(milliseconds);

            }
        }
    }
}
