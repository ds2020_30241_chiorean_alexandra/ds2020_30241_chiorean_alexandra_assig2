﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProducerApp.Model
{
    public class Activity
    {
        public int PatientId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Name { get; set; }

        public Activity(int id, DateTime startDate, DateTime endDate, string name)
        {
            this.PatientId = id;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.Name = name;
        }
    }
}
